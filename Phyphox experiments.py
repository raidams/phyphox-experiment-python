import matplotlib.pyplot as plt
import pandas as pd

acceleration = 'Beschleunigung ohne g 2023-02-23 23-32-58.xls'
acceleration_Charger = 'Beschleunigung ohne g + Charger.xls'
gyroscope = 'Gyroskop Drehrate 2023-02-23 23-26-16.xls'
gyroscope_Charger = 'Gyroskop Drehrate + Charger.xls'

print("Select one of the following below by entering number!")
print()
options = ["1. Acceleration data", "2. Acceleration data with charger-charging",
           "3. Gyroscope data", "4. Gyroscope data with charger-charging"]
for x in options:
    print(x)

print()
value = int(input("Which one of these options would you like to plot? "))
print()
print("Select a vertical axis.")
valueXYZ = int(input("1. X   2. Y   3. Z   "))

fileName = [acceleration, acceleration_Charger, gyroscope, gyroscope_Charger]
title = ['Accelerometer data', 'Accelerometer charger-charging data',
         'Gyroscope data', 'Gyroscope charger-charging data']
y_axis = ""

if (value >= 0) and (value <= 2):
    match valueXYZ:
        case 1:
            y_axis = "Linear Acceleration x (m/s^2)"
        case 2:
            y_axis = "Linear Acceleration y (m/s^2)"
        case 3:
            y_axis = "Linear Acceleration z (m/s^2)"

elif (value > 2) and (value <= 4):
    match valueXYZ:
        case 1:
            y_axis = "Gyroscope x (rad/s)"
        case 2:
            y_axis = "Gyroscope y (rad/s)"
        case 3:
            y_axis = "Gyroscope z (rad/s)"

try:
    data = pd.read_excel(fileName[value - 1])
    columns = data[["Time (s)", y_axis]]
    columns.plot(x="Time (s)", y=y_axis)
    plt.title(title[value - 1])
    plt.show()
except:
    print("Your choice is invalid!")
